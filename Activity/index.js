
console.log("add users:");
let users = ["John", "Doe", "Tim", "Simon", "Cheng"];
console.log(users);

/*(Push and Pop (Stack Method)*/
function addUser(user) {
  users.push(user);
}

addUser("Norman");
console.log("After add:");
console.log(users);

// 4.
function getUsers(index) {
  return users[index];
}

let itemFound = getUsers(3);
console.log("Item or user found is " + itemFound);

// 5. (Push and Pop (Stack Method)
function deleteLastUsers() {
  let lastUser = users[users.length - 4];
  delete users[4];
  return lastUser;
}

let lastUser = deleteLastUsers();
console.log("the last user before delete was " + lastUser);

//6.

function updateUsers(index, user) {
  users[index] = user;
}

updateUsers(2, "Johnny");
console.log(users);

//7.

function deleteUsers() {
  users = [];
}

deleteUsers();
console.log(users);

// 8.

function checkEmptyArray() {
  if (users.length > 0) {
    return false;
  } else {
    return true;
  }
}

let isUsersEmpty = checkEmptyArray();
console.log(isUsersEmpty);
